# Changelog for News Feed Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v3.1.0]

- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]
- maven-parent 1.2.0
- maven-portal-bom 4.0.0

## [v3.0.0] - 2023-12-15

- News Feed portlet: remove elastic search client [#26194]

## [v2.8.4] - 2022-05-15

- Fixed changelog and README

## [v2.8.2] - 2019-09-22

- Feature: HTML markup is not only escaped as it was but also displayed as simple text
  
## [v2.8.1] - 2019-07-17

- Fixed bug, post editing leads to losing both formatting and mentions [#17181]

## [v2.8.0] - 2019-05-22

- Fixed Bug, Social networking: "See more" seems to reload a post with part of the old look and feel [#16724]
- Fixed Bug, News Feed: comments with links between parenthesis not recognised [#16673]

## [v2.7.2] - 2019-01-25

- News Feed avoid refresh page when user is commenting [#16205]
- Minor CSS fix for anchors links font size
- Bug fix, mentions and replies http links not working in some email notifications [#16225]
- Revise posts look and feel [#16452]

## [v2.6.0] - 2018-03-07

- Ported to GWT 2.8.2
- Fix for Incident #11187 citing a people (with '@') in comments is not working anymore
- Social-Networking - citing a people (with '@') in comments loses the focus [#11189]
- Allow to sort feeds per recent comments [#10192]
- Bug lack of blank space to separate the query term when hashtag is used [#7841]
- Support for ticket [#11139]
- Enhanced efficiency when retrieving mentioned users or groups in comments

## [v2.5.0] - 2017-11-13

- Fixes cannot see who liked posts on VREs of Parthenos [#10262]
- Add comment taking up to 3 seconds sometime to be delivered in the UI now shows a loader [#10242]
- Ported to GWT 2.8.1
  
## [v2.3.0] - 2017-02-20

- Fixes for changes to the underneath common-notification-library
- Minor css fix 
- Refactored to the new version of elastic search client's compliancy
- Do not apologies if no posts are present and try to engage the user to post something [#7212]
 
## [v2.2.0] - 2016-12-02

- emoved asl session
- Increased general performance and bugfixes 
- fetching of users list to mention in comments loaded on demand

## [v2.1.0] - 2016-10-29

- Support to show feeds related to user's statistics added
- Fixed time for comments/posts: the year is present only if the comment/post was made before the current one

## [v2.0.0] - 2016-06-29

- Updated for Liferay 6.2.5

## [v1.13.1] - 2016-02-29

- Full-text search supported
- Fix editing changes comment "metadata" namely data [#246]
- Multi-attachment supported
- Image preview available 

## [v1.10.0] - 2015-10-12

- Integrated workspace explorer widget and replace light tree
- Fix Post dates lack the year [#195]
- Revised the way we shorten posts' text when this is very long, better heuristic used
- Fixed bugUsers tagging does not work if @ is in the middle of already typed text, works for hashtags too [#320]
- Revised mail notification message formatting, user text is now more clear and visible
- Revised the way we provide back links for posts, removed assumption that News Feed is always present the default communitypage, good for single VRE portals support
		 
## [v1.9.0] - 2015-04-27

- Integrated gwt-bootstrap and revised css
- Ported to GWT 2.7.0

## [v1.8.0] - 2014-10-20

- Added support for hashtags
- fixed see more problem with commercial ands (amps;) not being converted
 
## [v1.7.1] - 2014-06-04

- Fixed bug that was allowing to like posts even if the user had the session expired.
- Added possibility to unlike alread liked posts
 
## [v1.6.4] - 2014-05-07

- Implemented the automatic scroll back in time for feeds (in VRE scope)
- Added possibility to unlike alread liked posts
- Added possibility to mention users in comments
- Added default comment inputbox at the bottom of feed comments, if any
- Added avatar replacement if user has no avatar
- Fixed double notifications for post owner who commented his post
- Fixed double notifications for post owner who liked his post
- Fixed user referral problem when post was deleted 
- Added session checking popup
- Fixed paste problem on replies
- Preserve new lines in comments implemented
- Moved to Java7
 
## [v1.4.0] - 2013-10-21

- Ported to GWT 2.5.1
- Ported to Feather Weight Stack
- Removed GCF Dependency
- Fix for support Ticket [#708]
- Fix for support Ticket [#636]


## [v1.1.0] - 2013-07-08

- Open single post separately enhancement implemented [#1818]
- links redirects correctly to user profiles

## [v1.0.0] - 2013-04-19

- Mavenized
- Change>Smart Refresh Support Added [#1539]
- Show HTTP URL in replies as HTML anchor links [#1542]
- Replies on App Feed exception fixed [#580]
- Add Tag people in News Feed Portlet [#1535]
- Notify people involved in post thread [#1576]
- Scope Dependent News Feed [#1561]
- Open single feed in new Window Support [#1599]

## [v0.1.0] - 2012-10-31

- First release 
