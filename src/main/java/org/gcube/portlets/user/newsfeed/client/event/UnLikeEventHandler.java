package org.gcube.portlets.user.newsfeed.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface UnLikeEventHandler extends EventHandler {
	  void onUnLike(UnLikeEvent event);
	}
